from selenium import webdriver
import openpyxl

def test_tableWriter():
    #Initial Setup
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://cosmocode.io/automation-practice-webtable/")
    driver.maximize_window()

    #Excel File Path
    filePath = "C:\\Users\\Diego\\Documents\\Trabajo\\TATA\\Cursos\\Selenium\\Code\\Quizzes\\04-10\\TableData.xlsx"

    #Opening the book
    book = openpyxl.load_workbook(filePath)
    #Selecting the worksheet TableData 
    sheet = book["TableData"]

    #Indentifying the tbody element of the table in the page.
    tableBody = driver.find_element_by_xpath("//tbody")

    #Get all the rows in the tbody, total 197
    #Used nested use of find_element method
    #Equal to xpath //tbody/tr
    tableRows = tableBody.find_elements_by_xpath("tr")

    #rowCount helper variable, useful for writing in Excel file
    rowCount = 1

    #Start of loop
    for row in tableRows:
        #//tbody/tr/td, 5 cells per row
        cells = row.find_elements_by_xpath("td")
        #Looping through each cell, 1-6 in Excel, 0-5 in the cells WebElement list, containing 5 <td>. 
        #The first Column 'Visited' is empty, skipping it by starting the loop in cellCount = 2.
        for cellCount in range(2,len(cells)+1):
            #Writing in Excel, using rowCount helper variable to stay in a particular row,
            #and cellCount to move through the 5 cells. 
            # ...column = cellCount-1... to start from 'A' column in Excel
            #... cells[cellCount-1].text... to start from index 1 of the <td> list. Skipping 'Visited' column
            sheet.cell(row = rowCount, column= cellCount-1).value = cells[cellCount-1].text
        #Moving to next row
        rowCount += 1
    
    #Saving changes in the workbook
    book.save(filePath)

    #Asserting the completion of the table in Excel
    assert "Zimbabwe" == sheet.cell(row = 197, column = 1).value

    #Closing the workbook
    book.close()

    #Closing Driver
    driver.close()


