from selenium import webdriver
import time

# 4 - Explain in what 3 scenarios driver.switch_to method can be used
# This method can be used in the following scenarios.
# - Changing the context of the driver to another window
# - Changing the context of the driver to a pop up alert
# - Changing the context of the driver to an iframe

# - Changing the context of the driver to another window
def test_switchToWindow():
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/windows")
    driver.maximize_window()

    openWindow = driver.find_element_by_link_text("Click Here")
    openWindow.click()
    time.sleep(1)
    #.window_handles is a list with all the windows IDs opened by Selenium
    #[0] is the parent window, the first opened by Selenium.
    #Then the rest of indexes are other windows opened.

    parentWindows = driver.window_handles[0]
    childWindow = driver.window_handles[1]

    #Using switch_to, to move to the recently opened window.
    driver.switch_to.window(childWindow)

    assert driver.find_element_by_tag_name("h3").text == "New Window"

    driver.quit()

def test_switchToAlert():
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/javascript_alerts")
    driver.maximize_window()

    alertButton = driver.find_element_by_xpath("//button[@onClick='jsAlert()']")
    confirmButton = driver.find_element_by_xpath("//button[@onClick='jsConfirm()']")
    promptButton =  driver.find_element_by_xpath("//button[@onClick='jsPrompt()']")

    alertButton.click()

    #Switching to the browser alert context
    alert = driver.switch_to.alert

    #One of the available methods
    alert.accept()

    assert "You successfully clicked an alert" in driver.find_element_by_id("result").text

    confirmButton.click()

    alert = driver.switch_to.alert
    #Other available method
    alert.dismiss()

    assert "You clicked: Cancel" in driver.find_element_by_id("result").text

    promptButton.click()

    alert = driver.switch_to.alert
    #Other relevant alert method
    alert.send_keys("In Alert")

    alert.accept()

    #Also possible to retrieve alert text with
    #alert.text

    assert "You entered: In Alert" in driver.find_element_by_id("result").text

    driver.quit()

    
# - Changing the context of the driver to an iframe

def test_switchToFrames():
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/iframe")
    driver.maximize_window()

    #Switch to frame using id/name/or index (0 being the first frame in the web app).
    driver.switch_to.frame("mce_0_ifr")
    #Search for the text area, inside the frame context, if looked up in the original context, script will fail.
    textArea = driver.find_element_by_id("tinymce")

    textArea.clear()
    textArea.send_keys("Inside the frame")

    time.sleep(1)

    #Used to return to original context
    #If the script doesn't return to the original context, it will try to search for the assert webElement 
    #inside the frame, failing the test
    driver.switch_to.default_content()

    assert "iFrame" in driver.find_element_by_tag_name("h3").text

    driver.quit()