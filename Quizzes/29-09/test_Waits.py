from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time

#5 - Which one is prefered Implicit or Explicit wait.
# It is considered best practice to use explicit waits when running into timeouts in our scripts.
# Implicit waits only wait for the appearance of certain elements on the page, while an explicit wait, alongside
# expected_conditions can be set to wait for a broader set of conditions. 


def test_explicitWait():
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/dynamic_controls")
    driver.maximize_window()

    enableButton = driver.find_element_by_xpath("//form[@id='input-example']/button")
    enableButton.click()

    #Setting up the WebDriverWait instance

    wait = WebDriverWait(driver,5)

    #Even though the input is there, it is disabled, therefore we need to use expected conditions, 
    #element to be clickable wait to interact with it. 
    wait.until(EC.element_to_be_clickable((By.XPATH,"//form[@id='input-example']/input")))

    input = driver.find_element_by_xpath("//form[@id='input-example']/input")

    input.send_keys("Enabled using Explicit Wait")

    assert input.get_attribute("value") == "Enabled using Explicit Wait"
    
    driver.quit()


def test_implicitWait():

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/dynamic_controls")
    driver.maximize_window()

    #Setting up an implicit wait on the script
    driver.implicitly_wait(5)

    enableButton = driver.find_element_by_xpath("//form[@id='input-example']/button")
    enableButton.click()

    input = driver.find_element_by_xpath("//form[@id='input-example']/input")

    input.send_keys("Enabled using Implicit Wait")

    assert input.get_attribute("value") == "Enabled using Implicit Wait"
    
    #Test fails because implicit wait is not fit for this case, the element is already there, but is not 
    #interactable.
    #Best use case for implicit wait is when we have an overall slow web application, and script
    #only needs for elements to appear.
    #For the rest of cases, it is better to use Explicit Waits

    driver.quit()

