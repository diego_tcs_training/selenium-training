from selenium import webdriver
import time 

#1 - What is StaleElement Exception in Selenium. Give Example.
# Thrown when a reference to an element is now “stale”.
# Stale means the element no longer appears on the DOM of the page.

#FROM DOCUMENTATION
#Possible causes of StaleElementReferenceException include, but not limited to:
#You are no longer on the same page, or the page may have refreshed since the element was located.
#The element may have been removed and re-added to the screen, since it was located. Such as an element being relocated. This can happen typically with a javascript framework when values are updated and the node is rebuilt.
#Element may have been inside an iframe or another context which was refreshed.

def test_staleException():

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/inputs")
    driver.maximize_window()
    #Locating this webElement in the page
    input = driver.find_element_by_xpath("//input[@type='number']")
    #Sending some keys
    input.send_keys("12345")
    time.sleep(1)

    #Refreshing the page
    driver.refresh()

    #After the page refresh, the driver has an obsolete reference of this web Element, it has become stale.
    #StaleElementReference Exception is going to be thrown
    
    try:
        input.send_keys("5678")
    except Exception as e:
        print(e)

    driver.quit()
