from selenium import webdriver
import time

#3- What is the difference between driver.close() and .quit()
    # The main difference is that .close() closes the current windows and 
    # .quit() quits the driver and close every associated window.

def test_CQ():
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/windows")
    driver.maximize_window()

    openWindow = driver.find_element_by_link_text("Click Here")
    openWindow.click()

    parentWindow = driver.window_handles[0]
    childWindow = driver.window_handles[1]

    #Moving to the new window.
    driver.switch_to.window(childWindow)
    time.sleep(2)
    #Closing this windows.
    driver.close()

    #Moving to the main window
    driver.switch_to.window(parentWindow)
    openWindow = driver.find_element_by_link_text("Click Here")
    #Opening the new window
    openWindow.click()
    time.sleep(2)
    #Closing the driver
    driver.quit()
