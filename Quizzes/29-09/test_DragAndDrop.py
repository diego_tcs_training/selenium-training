from selenium import webdriver
from seletools.actions import drag_and_drop
import time

# 2 - How do you implement drag and drop.
# Drag and Drop is implemented with the ActionChains class. 
# The ActionChains class is a way to automate low level interactions such as mouse movements, button actions
# key presses, and other interactions. Then we can chain these low level interactions to reach more complex
# actions like drag and drop or hover.

from selenium.webdriver.common.action_chains import ActionChains


def test_DnD():

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/drag_and_drop")
    driver.maximize_window()

    #Instance of the ActionChains class
    actions = ActionChains(driver)

    boxA = driver.find_element_by_css_selector("div[id='column-a']")
    boxB = driver.find_element_by_css_selector("div[id='column-b']")

    #For Drag And Drop, there are two available options, with WebElements (source, target) or with
    #an offset (source, xoffset, yoffset). 
    #In both cases, driver will hold down left mouse button on the source, and move it to the target, or the
    #desired coordinates.
    #However there is a known bug with some instances of the webdriver, that doesn't allow the ActionChains to
    #correctly perform the drag and drop when draggable is set to true.
    # https://github.com/SeleniumHQ/selenium/issues/8003
    # Thankfully there is a workaround library that fixes this for us called seletools, which uses JS code to simulate
    #the drag and drop using the DOM. 
    
    drag_and_drop(driver,boxA,boxB)

    time.sleep(2)

    assert boxA.text == "B"

    #Drag and Drop in ActionChains works perfectly otherwise.

    driver.get("https://jqueryui.com/droppable/")

    driver.switch_to.frame(0)

    dragDiv = driver.find_element_by_id("draggable")

    dropDiv = driver.find_element_by_id("droppable")

    actions.drag_and_drop(dragDiv, dropDiv).perform()

    assert dropDiv.text == "Dropped!"

    time.sleep(2)

    driver.quit()


