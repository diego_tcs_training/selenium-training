from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

#Most common Exceptions in Selenium

def test_timeOut():
    
     #Thrown when a command does not complete in enough time.
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    
    #Setting timeout to 5 seconds
    driver.set_page_load_timeout(1)
    #This page takes 30 seconds to load
    try:
        driver.get("https://the-internet.herokuapp.com/slow")
    except Exception as e:
        print(e)
    
    driver.quit()

def test_javascriptException():

    #An error occurred while executing JavaScript supplied by the user.
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://rahulshettyacademy.com/AutomationPractice/")
    driver.maximize_window()

    #Passed incorrect JS code
    try:
        driver.execute_script("window.scrollTo('String');")
    except Exception as e:
        print(e)

    driver.quit()

def test_elementNotInteractable():

    #Thrown when an element is present in the DOM but interactions 
    #with that element will hit another element.

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/dynamic_controls")
    driver.maximize_window()

    input = driver.find_element_by_xpath("//form[@id='input-example']/input")
    
    #Element cannot be interacted with, it is disabled
    try:
        input.send_keys("Hello")
    except Exception as e:
       print(e)

    driver.quit()

def test_noSuchElement():

    #Thrown when element could not be found.

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/dynamic_controls")
    driver.maximize_window()

    #Element doesn't exist
    try:
        div = driver.find_element_by_xpath("//div[@id='not-existing']")
    except Exception as e:
       print(e)

    driver.quit()

def test_staleElementReference():

    #Thrown when a reference to an element is now “stale”.
    #Stale means the element no longer appears on the DOM of the page.

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/inputs")
    driver.maximize_window()
   
    input = driver.find_element_by_xpath("//input[@type='number']")
  
    input.send_keys("12345")

    driver.refresh()
    
    #Element is no longer attached to current DOM
    try:
        input.send_keys("5678")
    except Exception as e:
        print(e)

    driver.quit()

def test_noSuchWindow():
    #Thrown when window target to be switched doesn’t exist.
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/windows")
    driver.maximize_window()

    #Identified but not clicked
    openWindow = driver.find_element_by_link_text("Click Here")

    try:   
        driver.switch_to.window("Window")
    except Exception as e:
        print(e)

    driver.quit()

def test_noSuchFrame():

    #Thrown when frame target to be switched doesn’t exist.

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/iframe")
    driver.maximize_window()

    #Doesn't exist
    try:
        driver.switch_to.frame("Frame")
    except Exception as e:
        print(e)

    driver.quit()

def test_noAlertPresent():

   #Thrown when switching to no presented alert.
    #This can be caused by calling an operation on the Alert() class when an alert is not yet on the screen.

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://the-internet.herokuapp.com/iframe")
    driver.maximize_window()

    #Doesn't exist
    try:
         alert = driver.switch_to.alert
    except Exception as e:
        print(e)

    driver.quit()

def test_moveTargetOutOfBounds():

    #Thrown when the target provided to the ActionsChains move() method is invalid, i.e. out of document.
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://jqueryui.com/draggable/")
    driver.maximize_window()

    driver.switch_to.frame(0)

    draggable = driver.find_element_by_id("draggable")

    #Dragging the element out of frame
    try:
        ActionChains(driver).drag_and_drop_by_offset(draggable, 500, 500).perform()
    except Exception as e:
        print(e)

    driver.quit()

def test_elementNotVisible():
    
    #Thrown when an element is present on the DOM, but it is not visible, and so is not able to be interacted with.
    #Most commonly encountered when trying to click or read text of an element that is hidden from view.

    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://rahulshettyacademy.com/AutomationPractice/")
    driver.maximize_window()

    hideButton = driver.find_element_by_id("hide-textbox")
    hideButton.click()

    hiddenInput = driver.find_element_by_id("displayed-text")

    try:
        hiddenInput.click()
    except Exception as e:
        print(e)

    driver.quit()

