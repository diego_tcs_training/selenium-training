from selenium import webdriver

#For full page screenshots, I'll be using a package called Selenium-Screenshot
from Screenshot import Screenshot_Clipping

def test_takeScreenshot():
    #Initial Setup  
    driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
    driver.get("https://cosmocode.io/automation-practice-webtable/")
    driver.maximize_window()

    #Take a screenshot of the entire page
    ob = Screenshot_Clipping.Screenshot()
    #Computing time proportional to size of the page
    #This method will take several screenshots, and stitch them together.
    ob.full_Screenshot(driver,save_path='./Screenshots', image_name='SS_Body.png')

    #Indentifying the tbody element of the table in the page.
    tableBody = driver.find_element_by_xpath("//tbody")

    #Get all the rows in the tbody, total 197
    #Used nested use of find_element method
    #Equal to xpath //tbody/tr
    tableRows = tableBody.find_elements_by_xpath("tr")

    #Start of loop
    for row in tableRows:
        #Get all the td(cells) elements in each tr(rows).
        cells = row.find_elements_by_xpath("td")
        for cell in cells:
            #If Mexico is found
            if cell.text == "Mexico":
                #Take a screenshot of the entire row and save it in same directory as 'ss.png'
                # screenshot(filename) is an WebElement method, therefore, it can be applied at the td, tr, and tbody level
                # Each screenshot will be focused on each WebElement, except for the tableBody, only a portion of the table 
                # will be visible in the screenshot.
                # Extension must always be .png
        
                cell.screenshot("./Screenshots/SS_Cell.png")
                row.screenshot("./Screenshots/SS_Row.png")
                tableBody.screenshot("./Screenshots/SS_Table.png")

                break
       
    
    #Closing Driver
    driver.close()
