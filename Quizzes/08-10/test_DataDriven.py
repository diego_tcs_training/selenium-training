import pytest
from TestData.RegisterData import RegisterData
from TestData.RegisterDataJSON import RegisterDataJSON
from PageObjects.RegisterPage import RegisterPage

from selenium import webdriver


class TestDataDriven():

    #Enabling data driven framework by getting test case data from excel file and passing it to the test script using
    # a parametrized fixture.
    #Data loader fixture will get 3 dictionaries with 3 different data sets.
    @pytest.fixture(params = RegisterData.getTestData())
    def getData_test_Excel(self,request):
        return request.param

    #This data loader will obtain the test case data from a JSON document
    @pytest.fixture(params = RegisterDataJSON.getTestData())
    def getData_test_JSON(self,request):
        return request.param

    def test_DataDrivenExcel(self,getData_test_Excel):
        #Initial Setup  
        driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
        driver.get("https://rahulshettyacademy.com/angularpractice/")
        driver.maximize_window()

        registerPage = RegisterPage(driver)

        registerPage.enterName(getData_test_Excel['Name'])

        registerPage.enterEmail(getData_test_Excel['Email'])
    
        registerPage.enterPassWord(getData_test_Excel['Password'])

        registerPage.selectFromDropDown(getData_test_Excel['Gender'])

        registerPage.enterDate(getData_test_Excel['Day'],getData_test_Excel['Month'],getData_test_Excel['Year'])
        
        registerPage.submitForm()

        assert "Success" in registerPage.getAlertText()

        driver.close()

    def test_DataDrivenJSON(self,getData_test_JSON):
        #Initial Setup  
        driver = webdriver.Chrome(executable_path='C:\\Users\\Diego\\Downloads\\Programas a Instalar\\2021\\Drivers\\chromedriver_win32\\chromedriver.exe')
        driver.get("https://rahulshettyacademy.com/angularpractice/")
        driver.maximize_window()

        registerPage = RegisterPage(driver)

        registerPage.enterName(getData_test_JSON['Name'])

        registerPage.enterEmail(getData_test_JSON['Email'])
    
        registerPage.enterPassWord(getData_test_JSON['Password'])

        registerPage.selectFromDropDown(getData_test_JSON['Gender'])

        registerPage.enterDate(getData_test_JSON['Day'],getData_test_JSON['Month'],getData_test_JSON['Year'])
        
        registerPage.submitForm()

        assert "Success" in registerPage.getAlertText()

        driver.close()