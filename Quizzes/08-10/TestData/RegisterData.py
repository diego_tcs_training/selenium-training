import openpyxl

class RegisterData:
    #This method will open the workbook, get the sheet and load all the data into a dictionary, which will then be
    #appenden in a list and returned.
    @staticmethod
    def getTestData():
        book = openpyxl.load_workbook('C:\\Users\\Diego\\Documents\\Trabajo\\TATA\\Cursos\\Selenium\\Code\\Quizzes\\08-10\\TestData\\TestData.xlsx')
        sheet = book['Register']
        dataList = []
        #Parsing through the relevant rows and columns of the Excel sheet
        for i in range(2,sheet.max_row+1):
            dataSetDict = {}
            for j in range(1,sheet.max_column+1):
                dataSetDict[sheet.cell(row=1,column=j).value] = sheet.cell(row=i,column=j).value
            dataList.append(dataSetDict)
        
        return dataList
        