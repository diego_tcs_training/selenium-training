from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

#POM class for the register page
class RegisterPage():

    def __init__(self,driver):
        self.driver = driver

    #WebElements
    nameInput = (By.NAME, "name")
    emailInput = (By.NAME, "email")
    passWordInput = (By.ID, "exampleInputPassword1")
    genderDropDown = (By.ID, "exampleFormControlSelect1")
    dateInput = (By.NAME, "bday" )
    submitButton = (By.XPATH, "//input[@value='Submit']")
    successAlert = (By.XPATH,"//div[@class='alert alert-success alert-dismissible']")


    #Methods

    def enterName(self, name):
        self.driver.find_element(*RegisterPage.nameInput).send_keys(name)

    def enterEmail(self, email):
        self.driver.find_element(*RegisterPage.emailInput).send_keys(email)

    def enterPassWord(self, pwd):
        self.driver.find_element(*RegisterPage.passWordInput).send_keys(pwd)

    def enterName(self, name):
        self.driver.find_element(*RegisterPage.nameInput).send_keys(name)

    def selectFromDropDown(self,gender):
        selector = Select(self.driver.find_element(*RegisterPage.genderDropDown))
        selector.select_by_visible_text(gender)
    
    def enterDate(self,day,month,year):
        datePicker = self.driver.find_element(*RegisterPage.dateInput)
        datePicker.send_keys(day)
        datePicker.send_keys(month)
        datePicker.send_keys(year)

    def submitForm(self):
        self.driver.find_element(*RegisterPage.submitButton).click()

    def getAlertText(self):
        return self.driver.find_element(*RegisterPage.successAlert).text


    

