
import openpyxl

class HomePageData:
    
    test_HomePage_data = [{"name": "Diego Rugerio", "email": "diego.rugerio@mail.com","gender": "Male"},{"name": "Jane Doe", "email": "jane.doe@mail.com", "gender": "Female"}]

    @staticmethod
    def getTestData(testCaseName):
        book = openpyxl.load_workbook('C:\\Users\\Diego\\Documents\\Trabajo\\TATA\\Cursos\\Selenium\\Code\\TestingFramework\\TestData\\PythonDemo.xlsx')
        sheet = book.active
        dataDict = {}
        for i in range(1,sheet.max_row+1):
            if sheet.cell(row=i,column=1).value == testCaseName :
                for j in range(2,sheet.max_column+1):
                    dataDict[sheet.cell(row=1,column=j).value] = sheet.cell(row=i,column=j).value

        return [dataDict]

        