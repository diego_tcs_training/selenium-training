#Data used in the Login test case.
#Implemented Excel Test Driven utility
import openpyxl

class LoginData:

    @staticmethod
    def getTestData(testCaseName):
        book = openpyxl.load_workbook('C:\\Users\\Diego\\Documents\\Trabajo\\TATA\\Cursos\\Selenium\\Code\\FrameworkExercise\\TestData\\TestData.xlsx')
        sheet = book.active
        dataDict = {}
        for i in range(1,sheet.max_row+1):
            if sheet.cell(row=i,column=1).value == testCaseName :
                for j in range(2,sheet.max_column+1):
                    dataDict[sheet.cell(row=1,column=j).value] = sheet.cell(row=i,column=j).value

        return [dataDict]